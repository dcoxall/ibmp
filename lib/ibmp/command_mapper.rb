module IBMP
  class CommandMapper

    attr_reader :bitmap

    def initialize
      @commands = Hash.new
      @bitmap = nil
    end

    def register_command(char, command)
      if @commands.has_key?(char)
        raise ArgumentError, "Command #{char.inspect} already exists"
      else
        @commands[char] = command
      end
    end

    def command(char)
      @commands[char] or raise ArgumentError, "Command #{char.inspect} not found"
    end

    def call(char, *arguments)
      res = command(char).call(@bitmap, *arguments)
      res.is_a?(Bitmap) ? @bitmap = res : res
    end

  end
end
