module IBMP
  class Bitmap

    def initialize(width, height)
      @width, @height = width, height
      @bit_matrix = Array.new(height) { Array.new(width, 'O') }
    end

    def clear
      @bit_matrix = @bit_matrix.map { |col| col.map { 'O' } }
    end

    def color(x, y)
      @bit_matrix[y-1][x-1]
    end

    def set_color(x, y, color)
      @bit_matrix[y-1][x-1] = color
    end

    def vertical_line(x, y_start, y_end, color)
      (y_start..y_end).each do |y|
        set_color(x, y, color)
      end
    end

    def horizontal_line(y, x_start, x_end, color)
      (x_start..x_end).each do |x|
        set_color(x, y, color)
      end
    end

    def fill_region(x, y, color)
      old = color(x, y)
      change_surrounding_if_matches(x, y, old, color)
    end

    def to_s
      @bit_matrix.map do |column|
        column.join(" ")
      end.join("\n")
    end

    private

    def change_surrounding_if_matches(x, y, old_color, new_color)
      return if x > @width || x < 1 || y > @height || y < 1
      if color(x, y) == old_color
        set_color(x, y, new_color)
        change_surrounding_if_matches(x-1, y, old_color, new_color)
        change_surrounding_if_matches(x+1, y, old_color, new_color)
        change_surrounding_if_matches(x, y-1, old_color, new_color)
        change_surrounding_if_matches(x, y+1, old_color, new_color)
      end
    end

  end
end
