#!/usr/bin/env ruby
lib = File.expand_path('../../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'ibmp'

def exit_with_error(message)
  $stdout.puts message
  exit(1)
end

unless instructions = ARGV.first
  exit_with_error("The first argument should be a path to the instruction file.")
end

commands = IBMP::CommandMapper.new

# I realise there are other ways to provide methods but I'm here now
# and it doesn't hurt anyone

# Create new bitmap
commands.register_command("I", ->(_, width, height) {
  IBMP::Bitmap.new(width.to_i, height.to_i)
})

# Clear
commands.register_command("C", ->(bitmap) {
  bitmap.clear
})

# Colour
commands.register_command("L", ->(bitmap, x, y, color) {
  bitmap.set_color(x.to_i, y.to_i, color)
})

# Vertical Colour
commands.register_command("V", ->(bitmap, x, range_start, range_end, color) {
  range = [range_start.to_i, range_end.to_i].sort
  bitmap.vertical_line(x.to_i, range[0], range[1], color)
})

# Horizontal Colour
# Not sure why the signature is slightly different to vertical line? w.r.t arg positions
commands.register_command("H", ->(bitmap, range_start, range_end, y, color) {
  range = [range_start.to_i, range_end.to_i].sort
  bitmap.horizontal_line(y.to_i, range[0], range[1], color)
})

# Region Fill
commands.register_command("F", ->(bitmap, x, y, color) {
  bitmap.fill_region(x.to_i, y.to_i, color)
})

# Print
commands.register_command("S", ->(bitmap) {
  puts bitmap.to_s
})

# Exit
commands.register_command("X", ->(_) {
  exit
})

File.new(instructions).each_line do |line|
  commands.call(*line.split(/\s+/))
end
