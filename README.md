# Interactive Bitmap Editor (IBMP)

A very simple gem that allows instruction files to be read and executed which
describe the creation of bitmaps.

## Installation

Add this line to your application's Gemfile:

    gem 'ibmp'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ibmp

## Usage

The primary usage is via the executable (`bin/ibmp.rb`)

The command reads input from a file which is the first argument. The available
commands are detailed below:

- `I M N` - Create a new M x N image with all pixels coloured white (O).
- `C` - Clears the table, setting all pixels to white (O).
- `L X Y C` - Colours the pixel (X,Y) with colour C.
- `V X Y1 Y2 C` - Draw a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).
- `H X1 X2 Y C` - Draw a horizontal segment of colour C in row Y between columns X1 and X2 (inclusive).
- `F X Y C` - Fill the region R with the colour C. R is defined as: Pixel (X,Y) belongs to R. Any other pixel which is the same colour as (X,Y) and shares a common side with any pixel in R also belongs to this region.
- `S` - Show the contents of the current image
- `X` - Terminate the session

## Testing

Tests are written in `rspec` so you can run either:

    $ bundle install
    $ bundle exec rspec # .. or
    $ bundle exec rake
