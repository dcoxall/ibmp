lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ibmp/version'

Gem::Specification.new do |spec|
  spec.name          = "ibmp"
  spec.version       = IBMP::VERSION
  spec.authors       = ["Darren Coxall"]
  spec.email         = ["darren@darrencoxall.com"]
  spec.summary       = %q{Interactive bitmap editor}
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
end
