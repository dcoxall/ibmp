require 'spec_helper'
require 'ibmp'

describe IBMP::Bitmap do

  subject { described_class.new(5, 5) }

  describe "#set_color" do
    it "will set the value/color of an entity in the matrix" do
      # origin
      expect {
        subject.set_color(1, 1, "B")
      }.to change { subject.color(1, 1) }.from("O").to("B")

      # origin
      expect {
        subject.set_color(5, 5, "C")
      }.to change { subject.color(5, 5) }.from("O").to("C")
    end
  end

  describe "#vertical_line" do
    it "allows you to color all points in a specified column within 2 points" do
      subject.vertical_line(
        2, # second column
        2, # second row to...
        4, # fourth row
        "D", # set to 'D'
      )
      {
        [2,1] => "O",
        [2,2] => "D",
        [2,3] => "D",
        [2,4] => "D",
        [2,5] => "O",
      }.each do |coord, color|
        expect(subject.color(coord[0], coord[1])).to eql(color)
      end
    end
  end

  describe "#horizontal_line" do
    it "allows you to color all points in a specified column within 2 points" do
      subject.horizontal_line(
        2, # second row
        2, # second column to...
        4, # fourth column
        "D", # set to 'D'
      )
      {
        [1,2] => "O",
        [2,2] => "D",
        [3,2] => "D",
        [4,2] => "D",
        [5,2] => "O",
      }.each do |coord, color|
        expect(subject.color(coord[0], coord[1])).to eql(color)
      end
    end
  end

  describe "#fill_region" do
    before {
      # O O O O O
      # O D O D D
      # O D D D O
      # O O D O O
      # O O O D O
      subject.vertical_line(2, 2, 3, "D")
      subject.vertical_line(4, 2, 3, "D")
      subject.vertical_line(3, 3, 4, "D")
      subject.set_color(4, 5, "D")
      subject.set_color(5, 2, "D")
    }

    it "only colors points connected via the same color" do
      expect {
        subject.fill_region(3, 3, "A")
      }.to change {
        [
          subject.color(3, 3),
          subject.color(5, 2),
          subject.color(2, 2),
          subject.color(4, 5),
          subject.color(3, 2),
        ]
      }.from(%w(D D D D O)).to(%w(A A A D O))
    end
  end

  describe "#clear" do
    before { subject.set_color(1, 1, "D") }

    it "resets all colours to O" do
      expect {
        subject.clear
      }.to change { subject.color(1, 1) }.from("D").to("O")
    end
  end

  describe "#to_s" do
    before {
      subject.set_color(1, 1, "D")
      subject.set_color(2, 2, "A")
      subject.set_color(3, 3, "Z")
      subject.set_color(4, 4, "I")
      subject.set_color(5, 5, "K")
    }

    it "will return a string representing the bitmap" do
      string = [
        'D O O O O',
        'O A O O O',
        'O O Z O O',
        'O O O I O',
        'O O O O K',
      ].join("\n")

      expect(subject.to_s).to eql(string)
    end
  end

end
