require 'spec_helper'
require 'ibmp'

describe IBMP::CommandMapper do

  subject { described_class.new }

  let(:bitmap) { IBMP::Bitmap.new(5, 5) }

  let(:command) do
    proc { |*args|
      # do nothing
    }
  end

  describe "#register_command" do

    it "will insert a new command into memory" do
      expect {
        subject.register_command("I", command)
      }.to change { subject.command("I") rescue nil }.
        from(nil).to(command)
    end

    it "throws an error if the command is already registered" do
      subject.register_command("I", command)
      expect {
        subject.register_command("I", command)
      }.to raise_error(ArgumentError)
    end
  end

  describe "#command" do
    it "will return a single block to execute if available" do
      subject.register_command("I", command)
      expect(subject.command("I")).to eql(command)
    end

    it "will raise an ArgumentError if command can't be found" do
      expect {
        subject.command("I")
      }.to raise_error(ArgumentError)
    end
  end

  describe "#call" do
    let(:command) do
      proc { |_, *args|
        args.join(", ")
      }
    end

    before { subject.register_command("I", command) }

    it "calls the command and passes in the arguments" do
      result = subject.call("I", "foo", "bar")
      expect(result).to eql("foo, bar")
    end

    it "stores the result if the result is a Bitmap" do
      subject.register_command("Z", ->(_) { bitmap })
      expect {
        subject.call("Z")
      }.to change(subject, :bitmap).from(nil).to(bitmap)
    end
  end

  describe "#bitmap" do
    it "returns a Bitmap instance if created" do
      subject.register_command("I", -> (_, w, h) { IBMP::Bitmap.new(w, h) })
      subject.call("I", 5, 5)
      expect(subject.bitmap).to be_a(IBMP::Bitmap)
    end

    it "returns nil if no Bitmap created" do
      expect(subject.bitmap).to be_nil
    end
  end

end
